// EXPRESS SETUP
// 1. Import by using the 'require' directive to get access to the components of express package/depency
const express = require('express')
// 2. Use the express() function and assign in to an app variable  tp create an express app or app server
const app = express()
// 3. Declare a variable for the port of the server
const port = 5000

// Middlewares
// 4. These two .use are essential in express
// Allows your app to reas json format data
app.use(express.json())
// Allows your app to read data from forms.
app.use(express.urlencoded({extended: true}))


// 5.You can then have your routes after setting up Express
// Routes
// Express has methods corresponding to each HTTP method/s
// The full base URI for the local app for the routes will be at "http://localhost:3000"

// Get request route
app.get('/', (request, response) => {
	// once the route is accessed it will then send a string response conataining "Hello World"
	response.send('Hello World')
})

// This route expects to receive a GET request at the URI "/hello"
app.get('/hello', (request, response) => {
	response.send("Hello from /hello endpoint!")
})


// Register user route

// An array that will store user objects/documents when the "/register" route is accessed
// This will also serve as our mock database
let users = [
{
	"username": "Loren",
	"password": "loren123"
},
{
	"username": "Maria",
	"password": "maria123"
},
{
	"username": "Dela",
	"password": "dela123"
},
{
	"username": "Cruz",
	"password": "cruz123"
},
{
	"username": "batch",
	"password": "batch197"
},
{
	"username": "zuitt",
	"password": "coding123"
}
];

// This route expects to receive a POST request at the URI "/register"
// This will create a suer object in the "users" variable that mirrors a real world registration process
app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " ") {
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

// Put request route
app.put('/change-password', (request, response) => {
	let message

	for(let i = 0; i < users.length; i++) {
		if(request.body.username == users[i].username){
			users[i].password == request.body.password
			message = `User ${request.body.username}'s password has been updated!`
			break
		} else {
			message = 'User does not exist.'
		}
	}
	response.send(message)
})

// Activty S34
// #home

app.get('/home', (request, response) => {
	response.send("Welcome to the home page!")
})

// #users
app.get('/users', (request, response) => {
	response.send(users)
})

// #delete
app.delete('/delete-user', (request, response) => {
	let message;
	for (x=0; x<=users.length; x++) {
		if(request.body.username == users[x].username){
			users.splice(x,1)
			message = (`User ${request.body.username} has been deleted`)
			break;
		} else {
			message = ("User not found")
		}
	}
	response.send(message)
})

app.listen(port, () => console.log(`Server is running at port ${port}`))